#!/usr/bin/env bash

echo "Let's check if we can run the whole process"
echo " Get example"
make get-example
echo " Perform TIMA"
make tima
echo " Perform TIMA without MS1"
make tima-noMS1

echo "We need to do quality controls, please help!"
