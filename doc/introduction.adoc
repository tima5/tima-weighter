== Introduction

This repository contains everything needed to perform taxonomically informed natural products annotation.
It currently takes annotations coming from:

- https://oolonek.github.io/ISDB/[ISDB]
- https://ccms-ucsd.github.io/GNPSDocumentation/nap/[NAP]

We are woking on other possible annotation tools as input, such as:

- https://bio.informatik.uni-jena.de/software/sirius/[Sirius]
- http://prime.psc.riken.jp/compms/msfinder/main.html[MSFinder]
- http://mzmine.github.io[MZmine]

If you would like another annotation tool to be included, please https://gitlab.unige.ch/Adriano.Rutz/taxoscorerevolved/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=[submit an issue].
