# Paths

## config
export DEFAULT_PATH = ${CONFIG_PATH}/default
export PARAMS_PATH = ${CONFIG_PATH}/params

## data
export INTERIM_PATH = ${DATA_PATH}/interim
export PROCESSED_PATH = ${DATA_PATH}/processed
export SOURCE_PATH = ${DATA_PATH}/source

## doc
export REFERENCES_PATH = ${DOC_PATH}/references

## src
export DOCOPT_PATH = ${SRC_PATH}/docopt
export R_PATH = ${SRC_PATH}/R

### INTERIM
export INTERIM_ADDUCTS_PATH = ${INTERIM_PATH}/adducts
export INTERIM_PRIVATE_PATH = ${INTERIM_PATH}/private

### PROCESSED
export PROCESSED_FIGURES_PATH = ${PROCESSED_PATH}/figures
export PROCESSED_PARAMS_PATH = ${PROCESSED_PATH}/params
export PROCESSED_PRIVATE_PATH = ${PROCESSED_PATH}/private

### SOURCE
export SOURCE_ADDUCTS_PATH = ${SOURCE_PATH}/adducts
export SOURCE_DATABASES_PATH = ${SOURCE_PATH}/databases
export SOURCE_DICTIONARIES_PATH = ${SOURCE_PATH}/dictionaries
export SOURCE_NEUTRAL_LOSSES_PATH = ${SOURCE_PATH}/neutral_losses
export SOURCE_PRIVATE_PATH = ${SOURCE_PATH}/private

### R
export FUNCTIONS_PATH = ${R_PATH}/functions
