export CONFIG_PATH ?= ${PWD}/config
export DATA_PATH ?= ${PWD}/data
export DOC_PATH ?= ${PWD}/doc
export IMAGES_PATH ?= ${PWD}/images
export SRC_PATH ?= ${PWD}/src
export TESTS_PATH ?= ${PWD}/tests
