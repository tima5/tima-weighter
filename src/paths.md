# paths

path_r_python = function(x, y = NULL) { 
  if (!is.null(y)) { 
    if (language == "r") { 
      path = file.path(x, y)
    }
    if (language == "python") {
      path = os.pathjoin(x, y)
    }
  return(path)
  }
  if (is.null(y)) { 
    if (language == "r") { 
      path = file.path(x)
    }
    if (language == "python") {
      path = os.pathjoin(x)
    }
    return(path)
  }
}

## config
config = path_r_python("../config")

### default

config_default = path_r_python(config, "default")

#### annotate  

config_default_annotate = path_r_python(config_default, "annotate_default.yaml")

### params

config_params = path_r_python(config, "params")

#### annotate  

config_params_annotate = path_r_python(config_params, "annotate_params.yaml")

## data

data = path_r_python("../data")

### source

data_source = path_r_python(data, "source")

#### adducts

data_source_adducts = path_r_python(data_source, "adducts")

##### adducts file

data_source_adducts_file = path_r_python(data_source_adducts, "adducts.tsv")

#### neutral losses

data_source_neutral_losses = path_r_python(data_source, "neutral_losses")

##### neutral losses file

data_source_neutral_losses_file = path_r_python(data_source_neutral_losses, "neutral_losses.tsv")

### dictionaries

data_source_dictionaries = path_r_python(data_source, "dictionaries")

#### ranks

data_source_dictionaries_ranks = path_r_python(data_source_dictionaries, "ranks.tsv")

### interim

data_interim = path_r_python(data, "interim")

##### db pos

data_interim_adducts_db_pos = path_r_python(data_interim, "db_prepared_pos.tsv.gz")

##### db neg

data_interim_adducts_db_neg = path_r_python(data_interim, "db_prepared_neg.tsv.gz")

##### pos

data_interim_adducts_pos = path_r_python(data_interim, "adducts_pos.tsv.gz")

##### neg

data_interim_adducts_neg = path_r_python(data_interim, "adducts_neg.tsv.gz")

### processed

data_processed = path_r_python(data, "processed")

#### figures 

data_processed_figures = path_r_python(data_processed, "figures")

## src

### docopt

#### annotate
docopt_annotate_features = "docopt/annotate_features.txt"
