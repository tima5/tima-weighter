include config.mk

.PHONY: get-example tima tima-noMS1
.PRECIOUS: %.tsv %.zip %.json %.gz

get-example:
	bash src/shell/get_example.sh
	
tima:
	cd src && Rscript R/tima.R

tima-noMS1:
	cd src && Rscript R/tima.R -c FALSE